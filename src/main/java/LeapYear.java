public class LeapYear
{
    public boolean isLeap(int number)
    {
        return isDivisible(number, 4) && !(isDivisible(number,100) && !isDivisible(number, 400));
    }

    private boolean isDivisible(int dividend, int divisor)
    {
        return dividend % divisor == 0;
    }
}
