import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class LeapYearShould
{
    private LeapYear leapYear;

    @Test
    public void not_be_leap_if_not_divisible_by_4()
    {
        int number = 1997;

        boolean leap = leapYear.isLeap(number);

        assertThat(leap, is(false));
    }

    @Test
    public void be_leap_if_divisible_by_4()
    {
        int number = 1996;

        boolean leap = leapYear.isLeap(number);

        assertThat(leap, is(true));
    }

    @Test
    public void be_leap_if_divisible_by_400()
    {
        int number = 1600;

        boolean leap = leapYear.isLeap(number);

        assertThat(leap, is(true));
    }

    @Test
    public void not_be_leap_if_divisible_by_100_but_not_by_400()
    {
        int number = 1800;

        boolean leap = leapYear.isLeap(number);

        assertThat(leap, is(false));
    }

    @Before
    public void setUp()
    {
        leapYear = new LeapYear();
    }
}
